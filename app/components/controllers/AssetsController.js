'use strict';

angular.module('assets')
  .controller('AssetsController', function ($scope, $rootScope, DTOptionsBuilder, DTColumnBuilder, DTColumnDefBuilder, $filter, $modal) {

    $scope.showModal = function(asset) {
      var scope =  $rootScope.$new();
      scope.asset = asset;
      $modal({
        template: 'components/templates/assetModal.html',
        show: true,
        persist: false, // destroy the new scope on hide
        backdrop: 'static',
        scope: scope
      });
    };


    var columns = [
      { "data": "tx_name" },
      { "data": "nm_trades" },
      { "data": "last_price" }
    ];

    $scope.dtOptions = {
      "dom": '<"wrapper"frtip>',
      "lengthMenu": [],
      "pageLength": 10,
      "searching": true,
      "processing": true,
      "language": {
        "processing": '<img src="/img/loading_small.gif" />'
      },
      "serverSide": true,
      "ajax": {
        "url": 'https://mynxt.info/api/0.1/public/index.php/assets',
        "dataSrc": 'data',
        "data": function (data) {
          var column = columns[data.order[0].column].data;

          var filter = {
            draw: data.draw,
            skip: data.start,
            limit: data.length,
            orderBy: column + ' ' + data.order[0].dir
          };

          if (data.search.value !== "") {
            filter.search = data.search.value;
          }

          return filter;
        }
      },
      "orderMulti": false,
      "order": [ 1, 'desc'],
      "fnCreatedRow": function(row, data, dataIndex) {
        $('button', row).unbind('click');
        $('button', row).bind('click', function() {
          $scope.$apply(function() {
            $scope.showModal(data);
          });
        });
      }
    };

  $scope.dtColumns = [
    DTColumnBuilder.newColumn('tx_name').withTitle('Asset').renderWith(function(data, type, full) {
      return '<button class="btn-link">' + data + '</button>';
    }),
    DTColumnBuilder.newColumn('nm_trades').withTitle('Trades').renderWith(function (data) {
      return $filter('number')(data);
    }),
    DTColumnBuilder.newColumn('last_price').withTitle('Last price').notSortable().renderWith(function (data, type, full) {
      return $filter('number')(full.last_price) + ' NXT';
    })
  ];
});

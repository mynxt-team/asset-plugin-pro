'use strict';

angular.module('assets')
  .controller('AssetController', function ($scope, $rootScope, $filter, MyNXT) {
    var asset = $scope.$parent.asset;

    $scope.step = 1 / Math.pow(10, asset.nm_decimals);

    $scope.assetBalance = 0;

    $scope.buy = {
      quantity: 0,
      price: 0,
      orders: [],
      transaction: ''
    };
    $scope.sell = {
      quantity: 0,
      price: 0,
      orders: [],
      transaction: ''
    };
    $scope.transfer = {
      quantity: 0,
      recipient: '',
      transaction: ''
    };

    $scope.buyAsset = function () {
      $scope.loading = true;

      var quantityQNT = $filter('times')($scope.buy.quantity, Math.pow(10, asset.nm_decimals));
      var priceNQT =  $filter('times')($scope.buy.price, 100000000);
      priceNQT = $filter('divide')(priceNQT,  Math.pow(10, asset.nm_decimals));

      console.log(quantityQNT);

      var data = {
        requestType: 'placeBidOrder',
        account: $rootScope.selectedAccount,
        asset: asset.tx_asset_id,
        quantityQNT: quantityQNT,
        quantity: $scope.buy.quantity,
        priceNQT: priceNQT,
        price: $scope.buy.price,
        deadline: 1440,
        asset_order_type: 'placeBidOrder'
      };

      console.log(data);

      MyNXT.sendTransaction(data, function (result) {
        $scope.loading = false;

        if(result && result.transaction) {
          $scope.buy.transaction = result.transaction;
          $rootScope.refresh();
        }

        $scope.$apply();
      });
    };

    $scope.sellAsset = function () {
      $scope.loading = true;

      var quantityQNT = $filter('times')($scope.sell.quantity, Math.pow(10, asset.nm_decimals));
      var priceNQT =  $filter('times')($scope.sell.price, 100000000);
      priceNQT = $filter('divide')(priceNQT,  Math.pow(10, asset.nm_decimals));

      var data = {
        requestType: 'placeAskOrder',
        account: $rootScope.selectedAccount,
        asset: asset.tx_asset_id,
        quantityQNT: quantityQNT,
        quantity: $scope.sell.quantity,
        priceNQT: priceNQT,
        price: $scope.sell.price,
        deadline: 1440,
        asset_order_type: 'placeAskOrder'
      };

      MyNXT.sendTransaction(data, function (result) {
        $scope.loading = false;

        if(result && result.transaction) {
          $scope.sell.transaction = result.transaction;
          $rootScope.refresh();
        }

        $scope.$apply();
      });
    };

    $scope.transferAsset = function () {
      $scope.loading = true;

      var data = {
        requestType: 'transferAsset',
        account: $rootScope.selectedAccount,
        asset: asset.tx_asset_id,
        quantityQNT: $scope.transfer.quantity * Math.pow(10, asset.nm_decimals),
        quantity: $scope.transfer.quantity,
        recipient: $scope.transfer.recipient,
        deadline: 1440
      };

      MyNXT.sendTransaction(data, function (result) {
        $scope.loading = false;

        if(result && result.transaction) {
          $scope.transfer.transaction = result.transaction;
          $rootScope.refresh();
        }

        $scope.$apply();
      });
    };

    MyNXT.nrsRequest('getAskOrders', { asset: asset.tx_asset_id })
      .success(function (result) {
          if(result && result.askOrders && result.askOrders.length) {
            for(var i = 0; i < 3; i++) {
              var askOrder = result.askOrders[i];

              if(askOrder) {
                $scope.buy.orders.push(askOrder);
              }
            }

            $scope.buy.price = $filter('formatPriceNQT')($scope.buy.orders[0].priceNQT, asset.nm_decimals);
          }
    });

    MyNXT.nrsRequest('getBidOrders', { asset: asset.tx_asset_id })
      .success(function (result) {
        if(result && result.bidOrders && result.bidOrders.length) {
          for(var i = 0; i < 3; i++) {
            var bidOrder = result.bidOrders[i];

            if(bidOrder) {
              $scope.sell.orders.push(bidOrder);
            }
          }

          $scope.sell.price = $filter('formatPriceNQT')($scope.sell.orders[0].priceNQT, asset.nm_decimals);
        }
    });

    $scope.$watch('selectedAccount', function () {
      $scope.assetBalance = 0;
      MyNXT.nrsRequest('getAccount', { account: $rootScope.selectedAccount })
        .success(function (result) {
          if(result && result.assetBalances) {
            var assetBalances = result.assetBalances;

            for(var i = 0; i < assetBalances.length; i++) {
              if(assetBalances[i].asset == asset.tx_asset_id || assetBalances[i].asset == "15461730429559027380") {
                $scope.assetBalance = $filter('formatBalanceNQT')(assetBalances[i].balanceQNT, asset.nm_decimals);
              }
            }
          }
        });
    });

    $scope.fillBuyOrder = function (data) {
      $scope.buy.price = $filter('formatPriceNQT')(data.priceNQT, asset.nm_decimals);
      $scope.buy.quantity = $filter('formatBalanceNQT')(data.quantityQNT, asset.nm_decimals);
    };

    $scope.fillSellOrder = function (data) {
      $scope.sell.price = $filter('formatPriceNQT')(data.priceNQT, asset.nm_decimals);
      $scope.sell.quantity = $filter('formatBalanceNQT')(data.quantityQNT, asset.nm_decimals);
    };
  });

'use strict';

angular.module('assets')
  .controller('AccountAssetsController', function ($scope, $rootScope, $routeParams, $modal, DTOptionsBuilder, MyNXT) {
    $scope.showModal = function(index) {
      var scope =  $rootScope.$new();
      scope.asset = $scope.assets[index];
      $modal({
        template: 'components/templates/assetModal.html',
        show: true,
        persist: false, // destroy the new scope on hide
        backdrop: 'static',
        scope: scope
      });
    };

    $scope.dtOptions = DTOptionsBuilder.newOptions().withDOM('<"wrapper"frtip>');

    $scope.$watch('selectedAccount', function () {
      $scope.totalValue = 0;
      $scope.assets = [];
    });

    $scope.$watchCollection('[selectedAccount, random]', function () {
      MyNXT.nrsRequest('getAccount', { account: $rootScope.selectedAccount })
        .success(function (result) {
        if(result && !result.errorCode && result.assetBalances) {
          var assets = result.assetBalances;
          var assetIds = '';

          for(var i = 0; i < assets.length; i++) {
            assetIds += assets[i].asset + ',';
          }

          MyNXT.queryExplorer('assets', { assetIds: assetIds })
            .success(function (result) {
            if(result && result.data) {
              var data = result.data;

              var totalValue = 0;

              for (var i = 0; i < data.length; i++) {
                for(var j = 0; j < assets.length; j++) {
                  if(assets[j].asset === data[i].tx_asset_id) {
                    data[i].balance = assets[j].balanceQNT / Math.pow(10, data[i].nm_decimals);

                    totalValue += data[i].balance * data[i].last_price;
                  }
                }
              }

              $scope.assets = data;
              $scope.totalValue = totalValue;
            }
          });

        } else {
          $scope.assets = [];
        }
      });
    });
  });

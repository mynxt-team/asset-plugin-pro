'use strict';

angular.module('assets')
  .controller('AccountOrdersController', function ($scope, $rootScope, $routeParams, DTOptionsBuilder, DTColumnDefBuilder, MyNXT, $q) {
    $scope.dtOptions = DTOptionsBuilder.newOptions()
      .withDOM('<"wrapper"rtip>');

    $scope.dtColumnDefs = [
      DTColumnDefBuilder.newColumnDef(0),
      DTColumnDefBuilder.newColumnDef(1),
      DTColumnDefBuilder.newColumnDef(2),
      DTColumnDefBuilder.newColumnDef(3).notSortable(),
    ];


    $scope.totalValue = 0;
    $scope.askOrders = [];
    $scope.bidOrders = [];
    $scope.cancelledTransaction = null;

    $scope.cancelBidOrder = function (orderId) {
      $scope.loading = true;

      var data = {
        requestType: 'cancelBidOrder',
        account: $rootScope.selectedAccount,
        order: orderId
      };

      MyNXT.sendTransaction(data, function (result) {
        $scope.loading = false;

        if(result && result.transaction) {
          $scope.cancelledTransaction = result.transaction;
          $rootScope.refresh();
        }

        $scope.$apply();
      });
    };

    $scope.cancelAskOrder = function (orderId) {
      $scope.loading = true;

      var data = {
        requestType: 'cancelAskOrder',
        account: $rootScope.selectedAccount,
        order: orderId
      };

      MyNXT.sendTransaction(data, function (result) {
        $scope.loading = false;

        if(result && result.transaction) {
          $scope.cancelledTransaction = result.transaction;
          $rootScope.refresh();
        }

        $scope.$apply();
      });
    };

    $scope.$watch('selectedAccount', function () {
      $scope.askOrders = [];
      $scope.bidOrders = [];
    });

    $scope.$watchCollection('[selectedAccount, random]', function () {
      $scope.cancelledTransaction = null;

      var bidOrders = [];
      var askOrders = [];
      var assetIds = [];

      $q.all([
        MyNXT.nrsRequest('getAccountCurrentAskOrders', { account: $rootScope.selectedAccount })
          .success(function (result) {
            if (result && result.askOrders) {
              askOrders = result.askOrders;
            }
          }),
        MyNXT.nrsRequest('getAccountCurrentBidOrders', { account: $rootScope.selectedAccount })
          .success(function (result) {
            if (result && result.bidOrders) {
              bidOrders = result.bidOrders;
            }
          })
      ])
        .then(function () {
          MyNXT.nrsRequest('getUnconfirmedTransactions', { account: $rootScope.selectedAccount })
            .success(function (result) {
              if(result && result.unconfirmedTransactions) {
                var transactions = result.unconfirmedTransactions;

                for(var i = 0; i < transactions.length; i++) {
                  var transaction = transactions[i];

                  // buy order
                  if(transaction.type == 2 && transaction.subtype == 3) {
                    transaction.attachment.unconfirmed = true;
                    assetIds.push(transaction.attachment.asset);
                    bidOrders.push(transaction.attachment);
                  }

                  // sell order
                  if(transaction.type == 2 && transaction.subtype == 2) {
                    transaction.attachment.unconfirmed = true;
                    assetIds.push(transaction.attachment.asset);
                    askOrders.push(transaction.attachment);
                  }

                  // sell order cancel
                  if(transaction.type == 2 && transaction.subtype == 4) {
                    for(var j = 0; j < askOrders.length; j++) {
                      var order = askOrders[j];

                      if(order.order == transaction.attachment.order) {
                        askOrders.splice(j,1);
                      }
                    }
                  }

                  // buy order cancel
                  if(transaction.type == 2 && transaction.subtype == 5) {
                    for(var j = 0; j < bidOrders.length; j++) {
                      var order = bidOrders[j];

                      if(order.order == transaction.attachment.order) {
                        bidOrders.splice(j,1);
                      }
                    }
                  }
                }
              }

              for (var i = 0; i < askOrders.length; i++) {
                if (assetIds.indexOf(askOrders[i].asset) === -1) {
                  assetIds.push(askOrders[i].asset);
                }
              }
              for (var i = 0; i < bidOrders.length; i++) {
                if (assetIds.indexOf(bidOrders[i].asset) === -1) {
                  assetIds.push(bidOrders[i].asset);
                }
              }

              assetIds = assetIds.join(',');

              MyNXT.queryExplorer('assets', { assetIds: assetIds })
                .success(function (result) {
                  if (result && result.data) {
                    var data = result.data;

                    for (var i = 0; i < data.length; i++) {
                      for (var j = 0; j < bidOrders.length; j++) {
                        if (bidOrders[j].asset === data[i].tx_asset_id) {
                          bidOrders[j].tx_name = data[i].tx_name;
                          bidOrders[j].nm_decimals = data[i].nm_decimals;
                        }
                      }
                      for (var j = 0; j < askOrders.length; j++) {
                        if (askOrders[j].asset === data[i].tx_asset_id) {
                          askOrders[j].tx_name = data[i].tx_name;
                          askOrders[j].nm_decimals = data[i].nm_decimals;
                        }
                      }
                    }

                    $scope.askOrders = askOrders;
                    $scope.bidOrders = bidOrders;
                  }
                });
            });
        });
    });
  });
